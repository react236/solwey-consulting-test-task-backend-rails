Rails.application.routes.draw do
    devise_for :users
    # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html
    get "pages/index"
    
    get "/items", to: "items#index"
    get "/items/:id", to: "items#show"
    post "/items", to: "items#create"
    patch "/items/:id", to: "items#update"
    delete "/items/:id", to: "items#destroy"

    get "/orders", to: "orders#index"
    get "/orders/:id", to: "orders#show"
    post "/orders", to: "orders#create"
    # patch "/orders/:id", to: "orders#update"
    # delete "/orders/:id", to: "orders#destroy"

    get "/users", to: "users#index"
    get "/users/:id", to: "users#show"
    # post "/users", to: "users#create"
    patch "/users/:id", to: "users#update"
    delete "/users/:id", to: "users#destroy"

    # Defines the root path route ("/")
    root "pages#index"
end
