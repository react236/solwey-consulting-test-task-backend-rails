class CreateOrders < ActiveRecord::Migration[7.0]
  def change
    create_table :orders do |t|
      t.integer :user_id
      t.decimal :amount, precision: 7, scale: 2

      t.timestamps
    end
  end
end
