Задание: сделать интернет-магазин на rails
Клиента делать НЕ на rails, а строго на react

1. Бд используем postgres.

2. Будет четыре таблицы. 
    1. Users
            First name
            Last name
            Email
            Password
            Role 

    2. Товары. Items
            Name
            Description
            Price

    3. Orders
            User_id
            Amount 

    4. Orders_description
            Order_id
            Item_id
            Quantity

Пошагово.
1. User регистрируется и логинится. Для этого использовать gem devise
2. Из базы товаров юзер отбирает товар (использовать поиск по таблице) и указывает количество
3. После окончания отбора юзер "оплачивает" заказ. При этом создаётся запись в таблице3 и расшифровывается эта запись несколькими записями в таблице4, связывая с таблицей3 через order_id.
4. Таблица3 связана с таблицей1 через user_id
5. В зависимости от роли юзера (поле role, значения admin или use) юзеру доступен разный функционал. 
    ADMIN
        Доступны к просмотру и редактированию таблицы users и items. 
    USER
        Доступны для редактирования только личные данные 
6. Когда юзер логинится, то он может запросить список своих заказов и раскрыть каждый из них для просмотра. 

Особой красоты не нужно.
Особого дизайна не требуется.
Срок выполнения до 24.10.2013.


1. Установка rails
2. Установка devise и devise_api
3. Настройка devise: https://dev.to/kevinluo201/how-to-setup-very-basic-devise-in-rails-7-55ia
4. Настройка devise_api: https://github.com/nejdetkadir/devise-api#example-api-requests

API
GET /items      - получить все товары
GET /items/:id  - получить товар по id
PATCH /items/:id - обновить товар по id
DELETE /items/:id - удалить товар по id

GET /orders     - заказы пользователя
POST /order     - новый ордер

GET /users
GET /users/:id   - информация о пользователе

https://timely-valkyrie-1052ce.netlify.app/
Backend: render.com
