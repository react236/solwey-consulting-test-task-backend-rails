class User < ApplicationRecord
    has_many :order, dependent: :destroy
    
    # Include default devise modules. Others available are:
    # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
    devise  :database_authenticatable,
            :registerable,
            :recoverable,
            :rememberable,
            :validatable,
            :api
end
