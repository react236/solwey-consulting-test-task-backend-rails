class Order < ApplicationRecord
    has_many :order_descriptions, dependent: :destroy
    belongs_to :user
end
