# user_role и user_id определены в application_controller

class OrdersController < ApplicationController
    before_action :set_order, only: [ :show, :update, :destroy ]

    def index
        # Разный список ордеров для разных пользователей
        if user_role == 'admin'
            @orders = Order.all
        elsif user_role == 'user'
            @orders = Order.where("user_id = :id", { id: user_id })
        else
            @orders = nil
        end
            
        if user_role != 'guest'
            @full_orders = []

            print @orders
            if @orders
                # Составляем ответ из содержимого двух таблиц - orders и order_description 
                @orders.each do |order|
                    @options = {}

                    @options[:id] = order[:id]
                    @options[:user_id] = order[:user_id]
                    @options[:email] = user_role === 'admin' ? User.find_by(id: order[:user_id]).email : ''
                    @options[:amount] = order[:amount]
                    @options[:details] = []

                    order.order_descriptions.each do |descr|
                        @details = {}
                        @details[:item_id] = descr[:item_id]
                        @details[:quantity] = descr[:quantity]
                        @options[:details] << @details
                    end

                    @full_orders << @options
                end
            end

            render json: @full_orders
        else
            render json: { error: 'Forbidden' }, status: :forbidden
        end
    end

    def show
        if user_role == 'admin' || user_id
            render json: @order
        else
            render json: { error: 'Forbidden' }, status: :forbidden
        end
    end

    # POST /orders
    def create
        if user_id == nil
            render json: @order.errors, status: :forbidden
        else
            @cart_items = params[:order]

            # Считаем amount
            @amount = 0
            @cart_items.each do |item|
                @price = Item.find(item[:id]).price
                @amount = @amount + item[:count]*@price
            end

            # Сперва создаем ордер
            @order = Order.new(user_id: user_id, amount: @amount)
            if @order.save
                # Создаем order_description для конкретной позиции из ордера
                @cart_items.each do |item|
                    @order_description = @order.order_descriptions.create(order_id: @order[:id], item_id: item[:id], quantity: item[:count])
                end
            else
                render json: @order.errors, status: :unprocessable_entity
            end
        end
    end

    private
        # Use callbacks to share common setup or constraints between actions.
        def set_order
            @order = Order.find(params[:id])
        end

        # Only allow a list of trusted parameters through.
        def order_params
            params.require(:order)
        end
end
