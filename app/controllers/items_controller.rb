class ItemsController < ApplicationController
    before_action :set_item, only: [ :show, :update, :destroy ]

    # GET /items
    def index
        print search_params
        @items = Item.where("name LIKE :name", { name: "%#{search_params[:q]}%" })

        render json: @items
    end

    # GET /items/2
    def show
        render json: @item
    end

    # POST /items
    def create
        if user_role === 'admin'
            @item = Item.new(item_params)

            if @item.save
                render json: @item, status: :created
            else
                render json: { errors: 'Unable to create item' }, status: :unprocessable_entity
            end
        else
            render json: { error: 'Forbidden' }, status: :forbidden
        end
    end

    # PATCH/PUT /items/1
    def update
        if user_role == 'admin'
            if @item.update(item_params)
                render json: @item
            else
                render json: { errors: 'Unable to create item' }, status: :unprocessable_entity
            end
        else
            render json: { error: 'Forbidden' }, status: :forbidden
        end
    end

    # DELETE /items/1
    def destroy
        if user_role == 'admin'
            @item.destroy
            render json: @item, status: :ok
        else
            render json: { error: 'Forbidden' }, status: :forbidden
        end
    end

    private
        # Use callbacks to share common setup or constraints between actions.
        def set_item
            @item = Item.find(params[:id])
        end

        # Only allow a list of trusted parameters through.
        def item_params
            params.require(:item).permit(:name, :description, :price, :q)
        end

        def search_params
            params.permit(:q)
        end
end
