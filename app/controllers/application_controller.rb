class ApplicationController < ActionController::API
    before_action :configure_permitted_parameters, if: :devise_controller?

    protected
        def configure_permitted_parameters
            devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name])
        end

        def user_role
            if current_devise_api_user == nil
                "guest"
            else
                current_devise_api_user.role
            end
        end

        def user_id
            if current_devise_api_user == nil
                nil
            else
                current_devise_api_user.id
            end
        end
end

