class UsersController < ApplicationController
    before_action :set_user, only: [ :show, :update, :destroy ]

    def index
        if user_role == 'admin'
            @users = User.all
            render json: @users
        else
            render json: { error: 'Forbidden' }, status: :forbidden
        end
    end

    def show
        if user_role == 'admin' || user_id == params[:id].to_i
            render json: @user
        else
            render json: { error: 'Forbidden' }, status: :forbidden
        end
    end

    # PATCH /users/1
    def update
        if user_role == 'admin' || user_id == params[:id].to_i
            if @user.update(user_params)
                render json: @user
            else
                render json: { error: 'Failed to update user' }, status: :unprocessable_entity
            end
        else
            render json: { error: 'Forbidden' }, status: :forbidden
        end
    end

    # DELETE /users/1
    def destroy
        if user_role == 'admin'
            @user.destroy
            render json: @user, status: :ok
        else
            render json: { error: 'Forbidden' }, status: :forbidden
        end
    end

    private
        # Use callbacks to share common setup or constraints between actions.
        def set_user
            @user = User.find(params[:id])
        end

        # Only allow a list of trusted parameters through.
        def user_params
            params.require(:user).permit(:first_name, :last_name, :email, :role)
        end
end
